"""Microsoft365 tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_microsoft_365.streams import UsersStream

STREAM_TYPES = [
    UsersStream,
]


class TapMicrosoft365(Tap):
    """Microsoft365 tap class."""

    name = "tap-microsoft-365"

    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("tenant_id", th.StringType, required=True),
        th.Property("refresh_token", th.StringType, required=True),
        th.Property(
            "api_url",
            th.StringType,
            default="https://graph.microsoft.com/v1.0",
            description="The url for the API service",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    TapMicrosoft365.cli()
