"""Stream type classes for tap-microsoft-365."""

from singer_sdk import typing as th

from tap_microsoft_365.client import Microsoft365Stream


class UsersStream(Microsoft365Stream):
    """Define custom stream."""

    name = "users"
    path = "/users"
    records_jsonpath = "$.value[*]"
    primary_keys = ["id"]
    replication_key = None
    schema = th.PropertiesList(
        th.Property("accountEnabled", th.BooleanType),
        th.Property("ageGroup", th.StringType),
        th.Property("assignedLicenses", th.CustomType({"type": ["array", "string"]})),
        th.Property("assignedPlans", th.CustomType({"type": ["array", "string"]})),
        th.Property("businessPhones", th.CustomType({"type": ["array", "string"]})),
        th.Property("city", th.StringType),
        th.Property("companyName", th.StringType),
        th.Property("consentProvidedForMinor", th.StringType),
        th.Property("country", th.StringType),
        th.Property("createdDateTime", th.DateTimeType),
        th.Property("creationType", th.StringType),
        th.Property("department", th.StringType),
        th.Property("displayName", th.StringType),
        th.Property("employeeHireDate", th.DateTimeType),
        th.Property("employeeId", th.StringType),
        th.Property("employeeOrgData", th.CustomType({"type": ["object", "string"]})),
        th.Property("employeeType", th.StringType),
        th.Property("externalUserState", th.StringType),
        th.Property("externalUserStateChangeDateTime", th.DateTimeType),
        th.Property("faxNumber", th.StringType),
        th.Property("givenName", th.StringType),
        th.Property("identities", th.CustomType({"type": ["array", "string"]})),
        th.Property("imAddresses", th.CustomType({"type": ["array", "string"]})),
        th.Property("isResourceAccount", th.BooleanType),
        th.Property("jobTitle", th.StringType),
        th.Property("lastPasswordChangeDateTime", th.DateTimeType),
        th.Property("legalAgeGroupClassification", th.StringType),
        th.Property(
            "licenseAssignmentStates", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("mail", th.StringType),
        th.Property("mailNickname", th.StringType),
        th.Property("mobilePhone", th.StringType),
        th.Property("officeLocation", th.StringType),
        th.Property("onPremisesDistinguishedName", th.StringType),
        th.Property("onPremisesDomainName", th.StringType),
        th.Property(
            "onPremisesExtensionAttributes",
            th.CustomType({"type": ["object", "string"]}),
        ),
        th.Property("onPremisesImmutableId", th.StringType),
        th.Property("onPremisesLastSyncDateTime", th.DateTimeType),
        th.Property(
            "onPremisesProvisioningErrors", th.CustomType({"type": ["array", "string"]})
        ),
        th.Property("onPremisesSamAccountName", th.StringType),
        th.Property("onPremisesSecurityIdentifier", th.StringType),
        th.Property("onPremisesSyncEnabled", th.BooleanType),
        th.Property("onPremisesUserPrincipalName", th.StringType),
        th.Property("otherMails", th.CustomType({"type": ["array", "string"]})),
        th.Property("passwordPolicies", th.StringType),
        th.Property("passwordProfile", th.CustomType({"type": ["object", "string"]})),
        th.Property("postalCode", th.StringType),
        th.Property("preferredDataLocation", th.StringType),
        th.Property("preferredLanguage", th.StringType),
        th.Property("provisionedPlans", th.CustomType({"type": ["array", "string"]})),
        th.Property("proxyAddresses", th.CustomType({"type": ["array", "string"]})),
        th.Property("showInAddressList", th.BooleanType),
        th.Property("signInSessionsValidFromDateTime", th.DateTimeType),
        th.Property("state", th.StringType),
        th.Property("streetAddress", th.StringType),
        th.Property("surname", th.StringType),
        th.Property("usageLocation", th.StringType),
        th.Property("userPrincipalName", th.StringType),
        th.Property("userType", th.StringType),
        th.Property("mailboxSettings", th.CustomType({"type": ["object", "string"]})),
        th.Property("deviceEnrollmentLimit", th.NumberType),
        th.Property("aboutMe", th.StringType),
        th.Property("birthday", th.DateTimeType),
        th.Property("hireDate", th.DateTimeType),
        th.Property("interests", th.StringType),
        th.Property("mySite", th.StringType),
        th.Property("pastProjects", th.CustomType({"type": ["array", "string"]})),
        th.Property("preferredName", th.StringType),
        th.Property("responsibilities", th.CustomType({"type": ["array", "string"]})),
        th.Property("schools", th.CustomType({"type": ["array", "string"]})),
        th.Property("skills", th.CustomType({"type": ["array", "string"]})),
    ).to_dict()
